import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ShirtService } from '../services/shirt.service';
import { IonModal } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core/components';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
/**
 * Update
 */
  shirtName: string;
  shirtPrice: number;
  shirtUrl: string;
  
  constructor(private Shirt: ShirtService) { }

  /**
   * get All
   */
  shirts: any;
  ngOnInit() {
    this.Shirt.getAllShirts().subscribe(result => {
      this.shirts = result;

    })
  }

  /**
   * get One
   */
  ids: any
  getOneShirt(id: any) {
    this.Shirt.idTest = id
    this.Shirt.getOneShirt(id).subscribe(result => {
      this.ids = result;
    })
  }

  /**
   * Delete
   */
  deleteShirt(id: string) {
    this.Shirt.deleteShirt(id).subscribe(res => {
      console.log(res);
    })
  }

  /**
   * Create
   */
  object: any
  create() {
    this.Shirt.createShirt(this.object)
  }



}

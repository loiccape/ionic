import { Component, OnInit } from '@angular/core';
import { ShirtService } from '../services/shirt.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  constructor(private Shirt: ShirtService) { }

  ngOnInit() {
  }

  /**
   * variable qui vont etre update avec ngmodel
   */
  shirtName: string;
  shirtPrice: number;
  shirtUrl: string;

  shirtCreate() {
    let body = {
      name: this.shirtName,
      price: this.shirtPrice,
      url: this.shirtUrl
    }
    this.Shirt.createShirt(body).subscribe(res => {
      console.log(res);
    })
  }

}

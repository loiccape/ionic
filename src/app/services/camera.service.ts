import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera'
import { Filesystem, Directory } from '@capacitor/filesystem';

@Injectable({
  providedIn: 'root'
})
export class CameraService {

  picturito = [];

  constructor() { }

  /**
   * lors du clique en html
   * on lance la methode take photo qui attend que la camera 
   * envoie la photo avec getphoto()
   */
  public async takePhoto() {
    const photoTaken = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });
    /**on retourne la variable phototaken dans savePicture(filepath webviewpath)
     */
    await this.savePicture(photoTaken);
  }


  public async loadSaved() {
    /**
     * readdir retourne un liste de fichier 
     * il prend 2 parametres le path et le directory
     */
    const readFiles = await Filesystem.readdir({
      path: "",
      directory: Directory.Data
    })
    // Affichez la photo en lisant au format base64
    for (let photo of readFiles.files) {
      // Lire les données de chaque photo enregistrée à partir du système de fichiers
      const readFile = await Filesystem.readFile({
        path: photo.name,
        directory: Directory.Data,
      });

      this.picturito.unshift({
        filepath: photo.name,
        webviewPath: `data:image/jpeg;base64,${readFile.data}`
      })
    }
  }

  private async readAsBase64(photo: Photo) {
    // Récupérez la photo, lisez-la comme un blob, puis convertissez-la au format base64
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();

    return await this.convertBlobToBase64(blob) as string;
  }


  /**
   * 
   * @param photo la takenphoto
   */
  private async savePicture(photo: Photo) {
    // Convertir la photo au format base64, requis par l'API du système de fichiers pour enregistrer
    const base64Data = await this.readAsBase64(photo);

    // Écrire le fichier dans le répertoire de données
    const fileName = new Date().getTime() + '.jpeg';
    //sauvegarde la photo sur l'appareil
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      //Directory = trouve automatiquement le bon lieu de stockage
      directory: Directory.Data
    });



    // Utilisez webPath pour afficher la nouvelle image au lieu de base64 car c'est
    //déjà chargé en mémoire
    return {
      filepath: fileName,
      webviewPath: photo.webPath

    };
  }

  /**
   * 
   * @param 
   */
  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

}

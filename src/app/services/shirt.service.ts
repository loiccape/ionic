import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class ShirtService {

  idTest: any;

  constructor(private http: HttpClient) {
    this.http = http;
  }

  getAllShirts() {
    return this.http.get( environment.urlApi + "/Shirt/findAllShirts")
  }

  getOneShirt(id: any) {
    return this.http.get(environment.urlApi + `/Shirt/findShirtById/${id}`)
  }

  deleteShirt(id: any) {
    return this.http.delete(environment.urlApi + `/Shirt/deleteShirt/${id}`)
  }

  createShirt(Objet: any) {
    return this.http.post(environment.urlApi + `/Shirt/createShirt`, Objet)
  }

  updateShirt(id:any,Object: any) {
    return this.http.put(environment.urlApi + `/Shirt/updateShirt/${id}`, Object)
  }



}

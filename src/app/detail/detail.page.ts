import { Component, OnInit } from '@angular/core';
import { ShirtService } from '../services/shirt.service';
import { CameraService } from '../services/camera.service';
import { Photo } from '@capacitor/camera';
import { Directory, Filesystem } from '@capacitor/filesystem';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  constructor(private Shirt: ShirtService, private Camera: CameraService) { }

  test: any
  ngOnInit() {
    this.Camera.loadSaved();
    console.log(this.Camera.loadSaved());
    
    this.Shirt.getOneShirt(this.Shirt.idTest).subscribe(res => {
      this.test = res
    })
  }


  /**
   * Update
   */
  shirtName: string;
  shirtPrice: number;
  shirtUrl: string;

  updateShirt(id: string) {
    let body = {
      name: this.shirtName,
      price: this.shirtPrice,
      url: this.shirtUrl
    }
  

    this.Shirt.updateShirt(id,body).subscribe(res => {
      console.log(res);
      
    })
  }

}
